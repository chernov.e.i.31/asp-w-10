﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Threading.Tasks;
using System;
using HotChocolate;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class CustomerMutation
    {
        public async Task<CustomerResponse> CreateCustomerAsync(CreateOrEditCustomerRequest request, [Service] IRepository<Customer> customerRepository, [Service] IRepository<Preference> preferenceRepository)
        {
            //Получаем предпочтения из бд и сохраняем большой объект
            var preferences = await preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds);

            Customer customer = CustomerMapper.MapFromModel(request, preferences);

            await customerRepository.AddAsync(customer);

            return new CustomerResponse(customer);
        }

        public async Task<CustomerResponse> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request, [Service] IRepository<Customer> customerRepository, [Service] IRepository<Preference> preferenceRepository)
        {
            var customer = await customerRepository.GetByIdAsync(id);

            if (customer is not Customer)
                throw new GraphQLException("Customer not found");

            var preferences = await preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            CustomerMapper.MapFromModel(request, preferences, customer);

            await customerRepository.UpdateAsync(customer);

            return new CustomerResponse(customer);
        }

        public async Task<bool> DeleteCustomerAsync(Guid id, [Service] IRepository<Customer> customerRepository)
        {
            var customer = await customerRepository.GetByIdAsync(id);

            if (customer is not Customer)
                throw new GraphQLException("Customer not found");

            await customerRepository.DeleteAsync(customer);
            
            return true;
        }
    }
}
