﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using HotChocolate;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class CustomerQuery
    {
        public async Task<List<CustomerShortResponse>> GetCustomersAsync([Service] IRepository<Customer> customerRepository)
        {
            var customers = await customerRepository.GetAllAsync();

            return customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();
        }

        [HttpGet("{id:guid}")]
        public async Task<CustomerResponse> GetCustomerAsync(Guid id, [Service] IRepository<Customer> customerRepository)
        {
            var customer = await customerRepository.GetByIdAsync(id);

            if (customer is not Customer)
            {
                throw new GraphQLException("Customer not found");
            }

            return new CustomerResponse(customer);
        }
    }
}
